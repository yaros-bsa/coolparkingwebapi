﻿using System.Text.Json.Serialization;
using CoolParking.BL.Models;

namespace CoolParking.WebAPI.Models
{
    public class VehicleModel
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }
        [JsonPropertyName("vehicleType")]
        public VehicleType VehicleType { get; set; }
        [JsonPropertyName("balance")]
        public decimal Balance { get; set; }
    }
}

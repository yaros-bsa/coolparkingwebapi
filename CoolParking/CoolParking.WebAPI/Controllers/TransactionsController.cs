﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController : ControllerBase
    {
        private readonly Regex regex = new(@"[A-Z]{2}-\d{4}-[A-Z]{2}");

        private readonly IParkingService _parkingService;

        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }


        [HttpGet]
        [Route("last")]
        public ActionResult<IReadOnlyCollection<TransactionInfo>> GetLatsTransactions()
        {
            return Ok(_parkingService.GetLastParkingTransactions());
        }

        [HttpGet]
        [Route("all")]
        public ActionResult<string> AllTransactions()
        {
            if (!System.IO.File.Exists(Settings.LogPath))
                return NotFound("Log file not found");

            return Ok(_parkingService.ReadFromLog());
        }

        [HttpPut]
        [Route("topUpVehicle")]
        public ActionResult<Vehicle> TopUpVehicle([FromBody] TopUp topUp)
        {
            if (topUp == null)
                return BadRequest("NullReferenceExeption: TopUp");

            if (!regex.IsMatch(topUp.Id))
                return BadRequest("Invalid id");

            if (topUp.Sum <= 0)
                return BadRequest("The sum can not be less than zero or equals zero");

            Vehicle vehicle = Vehicle.GetVehicleById(topUp.Id);
            if (vehicle == null)
                return NotFound("Vehicle with this id did not found");

            _parkingService.TopUpVehicle(topUp.Id, topUp.Sum);

            return Ok(vehicle);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VehiclesController : ControllerBase
    {
        private readonly Regex regex = new(@"[A-Z]{2}-\d{4}-[A-Z]{2}");

        private IParkingService _parkingService { get; set; }

        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }


        [HttpGet]
        public ActionResult<IReadOnlyCollection<Vehicle>> GetVehiclesCollection()
        {
            return Ok(_parkingService.GetVehicles());
        }

        [HttpGet("{id}")]
        public ActionResult<Vehicle> GetVehiclesById(string id)
        {
            if (!regex.IsMatch(id))
                return BadRequest("Id is invalid");

            Vehicle vehicle = Vehicle.GetVehicleById(id);

            if (vehicle == null)
                return NotFound("Vehicle with this id is not found");

            return vehicle;
        }

        [HttpPost]
        public ActionResult<Vehicle> AddVehicle([FromBody] VehicleModel vehicleModel)
        {
            if (vehicleModel == null)
                return BadRequest("No one or several needed parameters");

            if (!regex.IsMatch(vehicleModel.Id))
                return BadRequest("Id is invalid");

            if (!Enum.IsDefined(vehicleModel.VehicleType))
                return BadRequest("Invalid Type vehicle");

            Vehicle vehicle;
            try
            {
                vehicle = new Vehicle(vehicleModel.Id, vehicleModel.VehicleType, vehicleModel.Balance);
                _parkingService.AddVehicle(vehicle);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Created(vehicle.Id, vehicle);
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteVehicles(string id)
        {
            if (!regex.IsMatch(id))
                return BadRequest("Id is invalid");

            Vehicle vehicle = Vehicle.GetVehicleById(id);

            if (vehicle == null)
                return NotFound("Vehicle with this id is not found");

            _parkingService.RemoveVehicle(id);

            return NoContent();
        }
    }
}

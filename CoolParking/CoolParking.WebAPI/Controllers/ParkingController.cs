﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ParkingController : ControllerBase
    { 
        private IParkingService _parkingService { get; set; }

        public ParkingController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }


        [HttpGet]
        [Route("capacity")]
        public ActionResult<int> GetCapacity()
        {
            return Ok(_parkingService.GetCapacity());
        }

        [HttpGet]
        [Route("balance")]
        public ActionResult<decimal> GetBalance()
        {
            return Ok(_parkingService.GetBalance());
        }

        [HttpGet]
        [Route("freePlaces")]
        public ActionResult<int> GetFreePlaces()
        {
            return Ok(_parkingService.GetFreePlaces());
        }

    }

}
